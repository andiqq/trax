# Trax
SWIFT 5 Trax demo from CS193p by Paul Hegarty

This is exactly the same DEMO Paul Hegarty is showing in CS193P.
The only change is that the SWIFT code has been changed to SWIFT 5

The project compiles and runs with iOS 12.2 on iPhone and iPad
